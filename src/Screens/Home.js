import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  View,
  FlatList,
} from "react-native";
import React, { useEffect, useState } from "react";
import { StatusBar } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import {
  getTop,
  getRecentlyPlayedSongs,
  newReleaseList,
  getUserPodcast,
} from "../../config/Auth";
import HomeNavBar from "../Components/Home/NavBar";
import Filter from "../Components/Filter";
import RecentList from "../Components/Home/RecentList";
import LargeCard from "../Components/Home/LargeCard";
import SmallCard from "../Components/Home/SmallCard";
import MediumCard from "../Components/Home/Mediumcard";
import AsyncStorage from "@react-native-async-storage/async-storage";

const Home = ({ navigation }) => {
  const [recentlyplayed, setRecentlyPlayed] = useState([]);
  const [recentlyplayeds, setRecentlyPlayeds] = useState([]);
  const [topArtists, setTopArtists] = useState([]);
  const [bestRecentList, setBestRecentList] = useState([]);
  const [newRelease, setNewRealease] = useState([]);
  const [myPodcasts, setPodcasts] = useState([]);

  useEffect(()=>{
    const checkAuth = async()=>{
      if(await AsyncStorage.getItem("token") == null){
        navigation.navigate('Login');
        console.log("Auth Log Out");
        return;
      }
    }
    checkAuth();
  });

  useEffect(() => {
    const fetchData = async () => {
      try {
        const [
          recentlyPlayedTrack,
          recentlyPlayedTracks,
          topArtists,
          tracksSuggests,
          newReleases,
          userPodcasts,
        ] = await Promise.all([
          getRecentlyPlayedSongs(6),
          getRecentlyPlayedSongs(15),
          getTop("artists"),
          getTop("tracks"),
          newReleaseList(),
          getUserPodcast(),
        ]);

        setRecentlyPlayed(recentlyPlayedTrack);
        setRecentlyPlayeds(recentlyPlayedTracks);
        setTopArtists(topArtists);
        setBestRecentList(tracksSuggests);
        setNewRealease(newReleases);
        setPodcasts(userPodcasts);
        // console.log(recentlyplayeds);
      } catch (error) {
        console.error(error);
      }
    };

    fetchData();
    console.log("Refreshed");
  }, []);

  const renderRecents = ({ item,index }) => {
    return (
      <RecentList
        key={item.id}
        title={item.track.name}
        imageUrl={item.track.album.images[0].url}
        onPress={() =>
          navigation.navigate("PlaySong", { songId: item?.track?.id })
        }
      />
    );
  };
  return (
    <SafeAreaView style={styles.safeAreaView}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <HomeNavBar
          PressNotification={() => navigation.navigate("Notification")}
          PressHistory={() => navigation.navigate("SongHistory")}
          PressSettings={() => navigation.navigate("Settings")}
        />

        <Filter Filter1="Music" Filter2="Podcasts & Shows" />

        <FlatList
          data={recentlyplayed}
          renderItem={renderRecents}
          numColumns={2}
          columnWrapperStyle={{ justifyContent: "space-between" }}
        />
        {/* <LargeCard title="All Time Favourites" data={recentlyplayed}/> */}

        <LargeCard title="All Time Favourites" data={bestRecentList} song ={true}/>

        <SmallCard title="Recently played" data={recentlyplayeds} song={true}/>

        <LargeCard title="Best of Artists" data={topArtists} artist={true}/>

        <LargeCard title="Popular new releases" data={newRelease} album={true}/>
        {/* {console.log(newRelease)} */}
        <MediumCard title="Your Podcasts" data={myPodcasts} />

        <View style={{ height: 20 }} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: "#121212",
    flex: 1, // Make the SafeAreaView expand to fill the available space
    padding: 20,
    paddingTop: StatusBar.currentHeight, // Adjust for the StatusBar height
  },
  normalText: {
    color: "white",
    fontSize: 16,
  },
});
