import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  Button,
  View,
  TouchableHighlight,
  TouchableOpacity,
} from "react-native";
import React, { useEffect, useState } from "react";
import { StatusBar } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import Icon from "react-native-vector-icons/FontAwesome";
import { Slider } from "native-base";
import { Audio } from "expo-av";
import { fetchSong } from "../../config/Auth";

const PlaySong = ({ route, navigation }) => {
  const { songId } = route.params;

  const [sound, setSound] = useState(null);
  const [isPlaying, setIsPlaying] = useState(false);
  const [position, setPosition] = useState(0);
  const [duration, setDuration] = useState(0);
  const [songInfo, setSongInfo] = useState([]);
  const [currentTime, setCurrentTime] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const info = await fetchSong(songId); // Replace this with your API call
        setSongInfo(info);

        // Initialize the audio sound
        const soundObject = new Audio.Sound();
        await soundObject.loadAsync({ uri: info.preview_url });
        setSound(soundObject);
        setDuration(info.duration_ms);
      } catch (error) {
        console.error("Error fetching song:", error);
      }
    };

    fetchData();
  }, []);

  const formatTime = (milliseconds) => {
    const totalSeconds = Math.floor(milliseconds / 1000);
    const hours = Math.floor(totalSeconds / 3600);
    const minutes = Math.floor((totalSeconds % 3600) / 60);
    const seconds = totalSeconds % 60;

    if (hours > 0) {
      return `${hours}:${minutes < 10 ? "0" : ""}${minutes}:${
        seconds < 10 ? "0" : ""
      }${seconds}`;
    } else {
      return `${minutes}:${seconds < 10 ? "0" : ""}${seconds}`;
    }
  };

  async function playSound() {
    try {
      if (sound) {
        if (isPlaying) {
          await sound.pauseAsync();
        } else {
          // Pause the currently playing song (if any)
          if (isPlaying) {
            await sound.pauseAsync();
          }
          // Play the new song
          await sound.playAsync();
        }
        setIsPlaying(!isPlaying);
      }
    } catch (error) {
      console.error("Error toggling play:", error);
    }
  }

  useEffect(() => {
    if (sound) {
      const interval = setInterval(async () => {
        const playbackStatus = await sound.getStatusAsync();
        if (playbackStatus.isLoaded) {
          setPosition(playbackStatus.positionMillis);
          setCurrentTime(playbackStatus.positionMillis); // Update current time
          setIsPlaying(playbackStatus.isPlaying); // Update isPlaying based on playback status
        }
      }, 1000);

      return () => {
        clearInterval(interval);
      };
    }
  }, [sound]);

  // const onSlidingComplete = async (value) => {
  //   if (sound) {
  //     await sound.setPositionAsync(value);
  //     if (isPlaying) {
  //       sound.playAsync();
  //     }
  //     setPosition(value); // Update position after seeking
  //   }
  // };
  const onSlidingComplete = async (value) => {
    if (sound) {
      const newPosition = value * duration /1000; // Convert value to milliseconds
      await sound.setPositionAsync(newPosition);
      if (isPlaying) {
        sound.playAsync();
      }
      setPosition(newPosition); // Update position after seeking
    }
  };

  return (
    <LinearGradient
      colors={["#3A3A4D", "#020202"]}
      style={{
        flex: 1,
        paddingTop: StatusBar.currentHeight / 2,
        paddingHorizontal: 25,
      }}
      className="justify-center "
    >
      <StatusBar backgroundColor="transparent" barStyle="light-content" />
      <ScrollView showsVerticalScrollIndicator={false}>
        {/* Nav Bar */}
        <TouchableOpacity
          onPress={() => navigation.navigate("Main")}
          className="flex-row items-center justify-between py-3 pt-8"
        >
          <Icon name="chevron-down" size={20} color="white" />
          <View className="flex-col items-center">
            <Text className="text-white text-xs">PLAYING FROM PLAYLIST</Text>
            <Text className="text-white text-xs font-black">
              Tamil Unplugged
            </Text>
          </View>

          <Icon name="ellipsis-v" size={20} color="white" />
        </TouchableOpacity>

        <View
          className="items-center justify-center w-full my-8"
          style={{ height: 500, padding: 10 }}
        >
          <Image
            source={{ uri: songInfo?.album?.images[0]?.url }}
            style={{ width: "100%", height: 350 }}
          />
        </View>

        <View className="flex-row justify-between">
          <View>
            <Text className="text-white font-black text-xl">
              {songInfo?.name}
            </Text>
            <Text className="text-stone-400 font-bold text-sm">
              {songInfo?.artists?.map((item, index) => item.name + ", ")}
            </Text>
          </View>
          <Icon name="heart-o" size={25} color="white" />
        </View>
        <View className="mt-5">
          {/* {console.log(duration)} */}
          <Slider
            value={position/1000}
            minimumValue={0}
            maxValue={duration/1000} 
            onSlidingComplete={onSlidingComplete}
            thumbTintColor="#00f"
            colorScheme="green"
          >
            <Slider.Track>
              <Slider.FilledTrack bg="green.600"/>
            </Slider.Track>
            <Slider.Thumb />
          </Slider>
          <View className="flex-row justify-between">
            <Text className="text-white">{formatTime(currentTime)}</Text>
            <Text className="text-white">{formatTime(duration)}</Text>
          </View>
        </View>

        <View className="flex-row justify-between items-center my-5">
          <Icon name="random" size={25} color="white" />
          <Icon name="step-backward" size={25} color="white" />
          <TouchableHighlight onPress={playSound}>
            <Icon
              name={isPlaying ? "pause-circle" : "play-circle"}
              size={70}
              color="white"
            />
          </TouchableHighlight>
          <Icon name="step-forward" size={25} color="white" />
          <Icon name="repeat" size={25} color="white" />
        </View>
      </ScrollView>
    </LinearGradient>
  );
};

export default PlaySong;
