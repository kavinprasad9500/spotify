import { StyleSheet, Text } from "react-native";
import React from "react";
import { LinearGradient } from "expo-linear-gradient";

const Premium = () => {
  return (
    <LinearGradient colors={["#1A1A1A", "#020202"]} style={{ flex: 1 }} className="justify-center ">
      <Text className="text-white text-center text-4xl">Available Soon...</Text>
      </LinearGradient>
  );
};

export default Premium;

const styles = StyleSheet.create({});
