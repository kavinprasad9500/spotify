import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  FlatList,
  Image,
} from "react-native";
import React, { useEffect, useState } from "react";
import { StatusBar } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import { Input } from "native-base";
import BrowseCard from "../Components/Search/BrowseCard";
import SearchNavBar from "../Components/Search/NavBar";
import { searchTrack, browseCategories } from "../../config/Auth";
import Touchable from "../Components/Touchable";

const Search = ({ navigation }) => {
  const [categories, setCategories] = useState([]);
  const [searchText, setSearchText] = useState(null);
  const [searchTracks, setSearchTracks] = useState([]);

  const Searchhandle = (text) => {
    setSearchText(text);
  };

  useEffect(() => {
    if (!searchText) {
      setSearchTracks([]);
      return;
    }
    let timeoutId;

    const fetchData = async () => {
      try {
        const encodedSearch = encodeURIComponent(searchText);
        const tracks = await searchTrack(encodedSearch);
        setSearchTracks(tracks);
        // console.log(searchTracks.tracks.items[0].name);
        //     // console.log(searchTracks.tracks.items[0].id);
        //     // console.log(searchTracks.tracks.items[0].artists[0].name);
        //     // console.log(searchTracks.tracks.items[0].type);
        //     // console.log(searchTracks.tracks.items[1].album.images[0].url)
        //
      } catch (error) {
        console.error(error);
      }
    };

    if (searchText) {
      // Clear the previous timeout and start a new one
      clearTimeout(timeoutId);
      timeoutId = setTimeout(fetchData, 500); // Adjust the delay as needed
    }

    return () => {
      clearTimeout(timeoutId); // Clear the timeout when component unmounts
    };
  }, [searchText]);

  useEffect(() => {
    const fetchCategories = async () => {
      try {
        const category = await browseCategories();
        setCategories(category);
        // getRecommended();
        // console.log(categories);
      } catch (error) {
        console.error(error);
      }
    };
    fetchCategories();
  }, []);

  const browseList = ({ item }) => {
    return <BrowseCard name={item?.name} image={item?.icons[0]?.url} />;
  };

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <StatusBar backgroundColor="#121212" barStyle="light-content" />
      {/* Nav Bar */}
      <SearchNavBar />

      <Input
        className="bg-white text-base font-medium fixed"
        value={searchText}
        // style={{backgroundColor: 'white'}}
        onChangeText={Searchhandle}
        style={{ width: "100%" }}
        placeholder="What do you want to listen to?"
      />
      <View className="mt-3">
        <ScrollView showsVerticalScrollIndicator={false}>
          {searchText ? (
            searchTracks?.tracks?.items?.map((item, index) => (
              <Touchable
                key={index}
                onPress={() =>
                  navigation.navigate("PlaySong", {
                    songId: item.id,
                  })
                }
              >
                <View className="w-full h-16 flex-row items-center m-1">
                  <Image
                    source={{
                      uri: item.album.images[0].url,
                    }}
                    style={{ width: 60, height: 60 }}
                  />
                  <View>
                    <Text
                      className="ml-4 text-white"
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {item?.name}
                    </Text>
                    <Text
                      className="ml-4 text-zinc-400 text-xs"
                      numberOfLines={1}
                      ellipsizeMode="tail"
                    >
                      {item?.artists[0]?.name}
                    </Text>
                  </View>
                </View>
              </Touchable>
            ))
          ) : (
            <>
              <Text className="text-white font-bold text-base my-2">
                Browse all
              </Text>

              <FlatList
                data={categories}
                renderItem={browseList}
                numColumns={2}
                columnWrapperStyle={{ justifyContent: "space-between" }}
              />
            </>
          )}

          <View style={{ height: 180 }} />
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default Search;

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: "#121212",
    flex: 1, // Make the SafeAreaView expand to fill the available space
    padding: 20,
    paddingTop: StatusBar.currentHeight, // Adjust for the StatusBar height
  },
  greetingText: {
    color: "white",
    fontSize: 24,
    fontWeight: "bold",
  },
  normalText: { 
    color: "white",
    fontSize: 16,
  },
});
