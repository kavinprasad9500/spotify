import { Image, SafeAreaView, StyleSheet, Text, View } from "react-native";
import React, { useEffect, useState } from "react";
import Touchable from "../Components/Touchable";

const LikedSongs = ({ route, navigation }) => {
  const { likeSongs } = route.params;
  const [likes, setlikes] = useState([]);

  useEffect(() => {
    setlikes(likeSongs.items);
  }, []);

  return (
    <SafeAreaView style={styles.safeAreaView}>
      {likes?.map((item, index) => (
        <Touchable
          onPress={() =>
            navigation.navigate("PlaySong", {
              songId: item?.track?.id,
            })
          }
        >
          <View className="w-full h-16 flex-row items-center m-1">
            <Image
              source={{
                uri: item?.track?.album?.images[0]?.url,
              }}
              style={{ width: 60, height: 60 }}
            />
            <View>
              <Text
                className="ml-4 text-white"
                numberOfLines={1}
                ellipsizeMode="tail"
              >
                {item?.track?.name}
              </Text>
              <Text
                className="ml-4 text-zinc-400 text-xs"
                numberOfLines={1}
                ellipsizeMode="tail"
              >
                {item.track?.artists?.map((items, index) => items.name + ", ")}
              </Text>
            </View>
          </View>
        </Touchable>
      ))}
    </SafeAreaView>
  );
};

export default LikedSongs;

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: "#121212",
    flex: 1, // Make the SafeAreaView expand to fill the available space
    padding: 20, // Adjust for the StatusBar height
  },
});
