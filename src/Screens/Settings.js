import {
  Image,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from "react-native";
import React, { useEffect, useState } from "react";
import { LinearGradient } from "expo-linear-gradient";
import { getFollowArtist, getUserPlayList, userProfile } from "../../config/Auth";
import List from "../Components/Library/List";
import MyButton from "../Components/MyButton";
import AsyncStorage from "@react-native-async-storage/async-storage";

const Settings = ({navigation}) => {
  const [profile, setProfile] = useState(null); // Initialize with null
  const [playlists, setPlaylists] = useState(null);
  const [follows, setFollows] = useState(null);

  const signOut = () =>{
    AsyncStorage.removeItem("token");
    AsyncStorage.removeItem("expirationDate");
    AsyncStorage.removeItem("user_id");
    AsyncStorage.removeItem("name");
    navigation.navigate('Login');
  }

  useEffect(() => {
    const fetchData = async () => {
      try {
        const data = await userProfile();
        setProfile(data);
        const list = await getUserPlayList();
        setPlaylists(list);
        const following = await getFollowArtist();
        setFollows(following?.artists?.total);
      } catch (error) {
        console.error("Error fetching user profile:", error);
      }
    };
    fetchData();
    // console.log(playlists);
  }, []);

  if (!profile) {
    // You can display a loading spinner or message here
    return (
      <SafeAreaView style={styles.safeAreaView}>
        {/* Loading content */}
      </SafeAreaView>
    );
  }

  // Todo : Set Loading Icon to display the content
  return (
    <SafeAreaView style={styles.safeAreaView}>
      <LinearGradient
        colors={["#E9321F", "rgba(0,0,0,0)"]}
        style={{ height: "20%", padding: 20 }}
      >
        <View className="flex-row mt-5 align-middle ">
          <Image
            className="rounded-full"
            style={{ width: 80, height: 80 }}
            source={{ uri: profile?.images[0]?.url }}
          />
          <View className="ml-4 mt-5">
            <Text className="text-white text-2xl font-bold">
              {profile?.display_name}
            </Text>
            <Text className="text-white">
              {" "}
              {profile?.followers?.total} Followers {"\t"} {follows} Following
            </Text>
          </View>
        </View>
      </LinearGradient>
      <View style={{ paddingHorizontal: 20 }}>
        <Text className="text-white font-semibold text-lg mb-3">Playlists</Text>
        <ScrollView showsVerticalScrollIndicator={false}>
          {playlists?.map((playlist, index) => (
            <List
              title={playlist.name}
              ImgUrl={playlist.images[0].url}
              type={playlist.type}
              desc={playlist.owner.display_name}
            />
          ))}
          <MyButton
          className="mt-5"
          variant="solid"
          title="Log out"
          backgroundColor="#1ed760"
          textColor="#121212"
          onPress={signOut}
        />
      
        </ScrollView>
      </View>
    </SafeAreaView>
  );
};

export default Settings;

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: "#121212",
    flex: 1,
    paddingTop: StatusBar.currentHeight, // Adjust for the StatusBar height
  },
});
