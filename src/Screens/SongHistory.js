import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
} from "react-native";
import React, { useEffect, useState } from "react";
import RecentList from "../Components/Home/RecentList";
import { getRecentlyPlayedSongs } from "../../config/Auth";

const SongHistory = ({ navigation }) => {
  const [recentlyplayed, setRecentlyPlayed] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const tracks = await getRecentlyPlayedSongs(20);
        setRecentlyPlayed(tracks);
      } catch (error) {
        console.log(error);
      }
    };
    fetchData();
  }, []);

  return (
    <SafeAreaView style={{ padding: 10 }}>
      <Text className="text-2xl text-white mb-5"> Recently Played Songs</Text>
      <ScrollView showsVerticalScrollIndicator={false}>
        {recentlyplayed.map((item, index) => (
          <RecentList
            style={{ width: "100%", marginVertical: 2 }}
            title={item.track.name}
            imageUrl={item.track.album.images[0].url}
            onPress={() =>
              navigation.navigate("PlaySong", { songId: item?.track?.id })
            }
          />
        ))}
        <View style={{ height: 60 }} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default SongHistory;
