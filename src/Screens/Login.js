import React, { useEffect } from "react";
import { Text, View, VStack } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import MyButton from "../Components/MyButton";
import { LinearGradient } from "expo-linear-gradient";
import { StatusBar } from 'react-native';
import { getAccessToken, getUser } from "../../config/Auth";
import * as WebBrowser from 'expo-web-browser';
import { makeRedirectUri, useAuthRequest } from 'expo-auth-session';
import AsyncStorage from "@react-native-async-storage/async-storage";

WebBrowser.maybeCompleteAuthSession();

  const discovery = {
    authorizationEndpoint: "https://accounts.spotify.com/authorize",
    tokenEndpoint: "https://accounts.spotify.com/api/token",
  };
const GetStart = ({ navigation }) => {

  useEffect(() => {
    const checkTokenValidity = async () => {
      const accessToken = await AsyncStorage.getItem("token");
      const expirationDate = await AsyncStorage.getItem("expirationDate");
      // console.log("access token : ",accessToken);
      // console.log("expiration date : ",expirationDate);

      if(accessToken && expirationDate){
        const currentTime = new Date().getTime();
        if(currentTime < parseInt(expirationDate)){
          // here the token is still valid
          navigation.replace("Main");
        } else {
          // token would be expired so we need to remove it from the async storage
          AsyncStorage.removeItem("token");
          AsyncStorage.removeItem("expirationDate");
        }
      }
      const user = await getUser();
      // console.log(user);
      if(user.id){
        AsyncStorage.setItem("user_id",user.id);
        AsyncStorage.setItem("name", user.display_name);
      }
      else{
        AsyncStorage.removeItem("user_id");
        AsyncStorage.removeItem("name");
      }
      
    }
    checkTokenValidity();
  },[]);

const [request, response, promptAsync] = useAuthRequest({
    clientId: 'a1583ea413ac4defbc120a8a5a86bca4',
    scopes: [
      "user-read-email",
      "user-library-read",
      "user-read-recently-played",
      "user-top-read",
      "user-follow-read",
      "playlist-read-private",
      "playlist-read-collaborative",
      "playlist-modify-public",
      "user-read-playback-position"
    ],
    usePKCE: false,
    redirectUri: makeRedirectUri({
      scheme: "exp://192.168.150.117:8081",
    }),
  },
  discovery
);

const Login = async () => {
  const result = await promptAsync();

  if (result.type === 'success') {
    const { code, state } = result.params;

    // console.log("Code : " + code + "\n" + "State : " + state);
    try {
      const token = await getAccessToken(code, state);
      if (token.access_token) {
        const expirationDate = Date.now() + 3600000; // Current time + 1 hour
        await AsyncStorage.multiSet([
          ['token', token.access_token],
          ['expirationDate', expirationDate.toString()],
        ]);

        navigation.navigate('Main');
      }
    } catch (error) {
      console.error('Error fetching access token:', error);
      Alert.alert('Authentication Error', 'An error occurred while fetching the access token.');
    }
  } else if (result.error) {
    Alert.alert(
      'Authentication Error',
      result.params.error_description || 'Something went wrong'
    );
  }
};

  return (
    <LinearGradient colors={["#1A1A1A", "#020202"]} style={{ flex: 1 }}>
      <StatusBar backgroundColor="#1A1A1A" barStyle={"light-content"} />

      <VStack space={2} alignItems="center" style={{ padding: 30 }}>
        <Icon
          name="spotify"
          size={80}
          color="#ffffff"
          style={{ marginVertical: "40%" }}
        />
        <Text
          color="#fff"
          alignItems="center"
          bold
          fontSize="4xl"
          lineHeight="sm"
        >
          Millons of songs.{"\n"}Free on Spotify.
        </Text>

        <View style={{ height: 80 }} />
        <MyButton
          variant="solid"
          title="Sign up for free"
          backgroundColor="#1ed760"
          textColor="#121212"
          onPress={Login}
        />
        <MyButton
          onPress={Login}
          variant="outline"
          title="Continue with phone"
          textColor="#fff"
          icon="phone"
        />
        <MyButton
          variant="outline"
          title="Continue with Google"
          textColor="#fff"
          icon="google"
          onPress={Login}
        />
        <MyButton
          variant="outline"
          title="Continue with Facebook"
          textColor="#fff"
          icon="facebook"
          onPress={Login}
        />
        <MyButton
          variant="unstyled"
          title="Log in"
          textColor="#fff"
           onPress={() => promptAsync()}
        />
      </VStack>
    </LinearGradient>
  );
};

export default GetStart;
