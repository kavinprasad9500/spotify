  import {
    Image,
    SafeAreaView,
    StatusBar,
    StyleSheet,
    Text,
    View,
  } from "react-native";
  import React, { useEffect, useState } from "react";
  import { getArtistTopTracks, getPlayListDetails } from "../../config/Auth";
  import { ScrollView } from "native-base";
  import Touchable from "../Components/Touchable";

  const PlayListInfo = ({ route, navigation }) => {
    const { playlist } = route?.params;
    const { artist } = route?.params;
    const playListId = playlist?.id;
    const artistId = artist?.id;

    const [playlistTracks, setPlaylistTracks] = useState([]);

    // useEffect(() => {
    //   const fetchData = async () => {
    //     const tracks = await getPlayListDetails(playListId);
    //     setPlaylistTracks(tracks?.items);
    //     // console.log(playListDetails[0].name);
    //     // console.log(playlistTracks.name);
    //     // console.log(playlistTracks.album.artists[0].name);
    //     // console.log(playlist);
    //     // console.log(playlistTracks);
    //   };
    //   fetchData();
    // }, [playlist]);

    // useEffect(() => {
    //   const fetchData = async () => {
    //     const track = await getArtistTopTracks(artistId);
    //     // console.log(artist);
    //     setPlaylistTracks(track?.tracks);
    //     // console.log(track.tracks[0].name);
    //     // console.log(track.tracks[0].id);
    //     // console.log(track.tracks[0]?.artists?.map((item, index) => item.name + ", "));
    //     // console.log(track.tracks[0].album.images[0].url);

    //     // console.log(playlistTracks[0].name);
    //     // console.log(playlistTracks[0].id);
    //     // console.log(playlistTracks[0].artists[0].name);
    //     // console.log(playlistTracks.album.artists[0].name);
    //     // console.log(playlist);
    //     // console.log(playlistTracks);
    //   };
    //   fetchData();
    // }, [artist]);

    
  useEffect(() => {
    // console.log(artist);
    const fetchData = async () => {
      if (artistId) {
        // Fetch artist top tracks only if artistId is available
        const track = await getArtistTopTracks(artistId);
        setPlaylistTracks(track?.tracks);
      } else if (playListId) {
        // Fetch playlist details only if playListId is available
        const tracks = await getPlayListDetails(playListId);
        setPlaylistTracks(tracks?.items);
      }
    };

    fetchData();
  }, [artistId, playListId]); 

    return (
      <SafeAreaView style={styles.safeAreaView} className="mt-10">
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            className="justify-center align-middle"
            style={{ alignItems: "center" }}
          >
            <Image
              source={{ uri: playlist?.images[0]?.url || artist?.images[0]?.url }}
              style={{ width: 200, height: 200 }}
            />
          </View>
          <Text className="text-white font-bold m-2 text-xl">
            {playlist?.name || artist?.name}
          </Text>
          {playlistTracks?.map((item, index) => (
            <Touchable
              onPress={() =>
                navigation.navigate("PlaySong", {
                  songId: item?.track?.id || item?.id,
                })
              }
            >
              <View className="w-full h-16 flex-row items-center m-1">
                <Image
                  source={{
                    uri:
                      item?.track?.album?.images[0]?.url ||
                      item?.album?.images[0]?.url,
                  }}
                  style={{ width: 60, height: 60 }}
                />
                <View>
                  <Text
                    className="ml-4 text-white"
                    numberOfLines={1}
                    ellipsizeMode="tail"
                  >
                    {item?.track?.name || item.name}
                  </Text>
                  <Text
                    className="ml-4 text-zinc-400 text-xs"
                    numberOfLines={1}
                    ellipsizeMode="tail"
                  >
                    {item?.track?.artists[0]?.name ||
                      item.artists?.map((items, index) => items.name + ", ")}
                  </Text>
                </View>
              </View>
            </Touchable>
          ))}
        </ScrollView>
      </SafeAreaView>
    );
  };

  export default PlayListInfo;

  const styles = StyleSheet.create({
    safeAreaView: {
      backgroundColor: "#121212",
      flex: 1, // Make the SafeAreaView expand to fill the available space
      padding: 20, // Adjust for the StatusBar height
    },
  });
