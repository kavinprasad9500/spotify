import {
  Image,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  Button,
  View,
} from "react-native";
import React, { useEffect, useState } from "react";
import { StatusBar } from "react-native";
import { LinearGradient } from "expo-linear-gradient";
import Icon from "react-native-vector-icons/FontAwesome";
import { getFollow, getUserPlayList, getUserPodcast, userLikedSongs } from "../../config/Auth";
import Filter from "../Components/Filter";
import Touchable from "../Components/Touchable";
import List from "../Components/Library/List";

const Library = ({navigation}) => {
  const [playlists, setPlaylists] = useState([]);
  const [followsArtists, setfollowsArtists] = useState([]);
  const [myPodcasts, setPodcasts] = useState([]);
  const [ likes, setLikes ] = useState([]);

  useEffect(() => {
    const likedSongs = async () =>{
      const likedSongs = await userLikedSongs();
      // console.log(likedSongs.total);
      setLikes(likedSongs);
    } 
    likedSongs();
    const playlist = async () => {
      const list = await getUserPlayList();
      // console.log(list);
      setPlaylists(list);
    };
    playlist();
    const follows = async () => {
      const followArtistData = await getFollow("artist");
      setfollowsArtists(followArtistData.artists.items);
      // console.log(followsArtists);
    };
    follows();
    const podcasts = async () => {
      const casts = await getUserPodcast();
      setPodcasts(casts);
    };
    podcasts();
  }, []);

  return (
    <SafeAreaView style={styles.safeAreaView}>
      <StatusBar backgroundColor="#121212" barStyle="light-content" />
      {/* Nav Bar */}
      <View className="flex-row items-center justify-between pt-5">
        <View className="flex-row items-center">
          <Image
            source={require("../../assets/image2.png")}
            style={{ width: 50, height: 50 }}
          />
          <Text style={styles.greetingText}>Your Library</Text>
        </View>
        <View className="flex-row items-center">
          <Icon
            name="search"
            size={25}
            color="white"
            style={{ marginLeft: 25 }}
          />
          <Icon
            name="plus"
            size={25}
            color="white"
            style={{ marginLeft: 25 }}
          />
        </View>
      </View>

      {/* Filters */}
      <Filter Filter1="Playlist" Filter2="Music" />

      <ScrollView showsVerticalScrollIndicator={false}>
        {/* todo : Liked Songs Playlist */}
        <Touchable onPress={() => navigation.navigate('LikedSongs', {likeSongs: likes})}>
          <View className="w-100 h-16 flex-row items-center m-1">
            <LinearGradient colors={["#33006F", "#FFFFFF"]}>
              <View
                style={{
                  width: 60,
                  height: 60,
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <Icon name="heart" size={24} color="white" />
              </View>
            </LinearGradient>
            <View>
              <Text className="ml-4 text-white">Liked Songs</Text>
              <Text className="ml-4 text-zinc-400 text-xs">
                📌 Playlist • {likes.total} songs
              </Text>
            </View>
          </View>
        </Touchable>


        {playlists?.map((playlist, index) => (
          <List
            title={playlist.name}
            ImgUrl={playlist.images[0].url}
            type={playlist.type}
            desc={playlist.owner.display_name}
            playlistDetails={playlist}
            playlist={true}
          />
        ))}
        {followsArtists?.map((artist, index) => (
          <List
            title={artist.name}
            ImgUrl={artist.images[0].url}
            type={artist.type}
            ImgStyle={{ borderRadius: 30 }}
            playlistDetails={artist}
            artist={true}
          />
        ))}
        {myPodcasts?.map((cast, index) => (
          <List
            title={cast.show.name}
            ImgUrl={cast.show.images[0].url}
            type={cast.show.type}
          />
        ))}

        <View style={{ height: 40 }} />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Library;

const styles = StyleSheet.create({
  safeAreaView: {
    backgroundColor: "#121212",
    flex: 1, // Make the SafeAreaView expand to fill the available space
    padding: 20,
    paddingTop: StatusBar.currentHeight, // Adjust for the StatusBar height
  },
  greetingText: {
    color: "white",
    fontSize: 24,
    fontWeight: "bold",
  },
  normalText: {
    color: "white",
    fontSize: 16,
  },
});
