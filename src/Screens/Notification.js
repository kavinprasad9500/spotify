import { StyleSheet, Text, View } from "react-native";
import React from "react";
import { LinearGradient } from "expo-linear-gradient";

const Notification = () => {
  return (
    <LinearGradient
      colors={["#1A1A1A", "#020202"]}
      style={{ flex: 1 }}
      className="justify-center "
    >
      <Text className="text-green-500 text-center text-4xl font-bold">Available Soon</Text>
    </LinearGradient>
  );
};

export default Notification;

const styles = StyleSheet.create({});
