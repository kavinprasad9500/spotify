import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import {
  createStackNavigator,
  CardStyleInterpolators,
} from "@react-navigation/stack";
import { StatusBar } from "expo-status-bar";
import React from "react";
import Login from "../../Screens/Login";
import Home from "../../Screens/Home";
import Search from "../../Screens/Search";
import Library from "../../Screens/Library";
import Premium from "../../Screens/Premium";
import Icon from "react-native-vector-icons/FontAwesome";
import { LinearGradient } from "expo-linear-gradient";
import PlaySong from "../../Screens/PlaySong";
import SongHistory from "../../Screens/SongHistory";
import Notification from "../../Screens/Notification";
import Settings from "../../Screens/Settings";
import PlayListInfo from "../../Screens/PlayListInfo";
import LikedSongs from "../../Screens/LikedSongs";

const Tab = createBottomTabNavigator();

const Bottomtabs = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: {
          backgroundColor: "rgba(0,0,0,0)",
          height: 70,
          position: "absolute",
          bottom: 0,
          left: 0,
          right: 0,
          shadowOpacity: 4,
          shadowRadius: 10,
          elevation: 0,
          shadowOffset: {
            width: 0,
            height: 4,
          },
          paddingTop: 0,
          paddingBottom: 0,
          borderTopWidth: 0,
          borderBottomWidth: 0,
        },
        tabBarBackground: () => (
          <LinearGradient
            colors={["rgba(0,0,0,0)", "rgb(0,0,0)"]} // Adjust colors as needed
            start={{ x: 0, y: 0 }}
            end={{ x: 0, y: 1 }}
            style={{
              position: "absolute",
              top: 0,
              left: 0,
              right: 0,
              bottom: 0,
            }}
          />
        ),
      }}
    >
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: "Home",
          headerShown: false,
          headerStyle: {
            backgroundColor: "#1A1A1A", // Set the background color for the app bar
            elevation: 0,
          },
          headerTintColor: "#fff",
          tabBarLabelStyle: { color: "white" },
          tabBarIcon: ({ focused }) =>
            focused ? (
              <Icon name="home" size={28} color="white" />
            ) : (
              <Icon name="home" size={25} color="#aaaaaa" />
            ),
        }}
      />
      <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarLabel: "Search",
          headerShown: false,
          tabBarLabelStyle: { color: "white" },
          tabBarIcon: ({ focused }) =>
            focused ? (
              <Icon name="search" size={28} color="white" />
            ) : (
              <Icon name="search" size={25} color="#aaaaaa" />
            ),
        }}
      />
      <Tab.Screen
        name="Library"
        component={Library}
        options={{
          tabBarLabel: "Your Library",
          headerShown: false,
          tabBarLabelStyle: { color: "white" },
          tabBarIcon: ({ focused }) =>
            focused ? (
              <Icon name="book" size={28} color="white" />
            ) : (
              <Icon name="book" size={25} color="#aaaaaa" />
            ),
        }}
      />
      <Tab.Screen
        name="Premium"
        component={Premium}
        options={{
          tabBarLabel: "Premium",
          headerShown: false,
          tabBarLabelStyle: { color: "white" },
          tabBarIcon: ({ focused }) =>
            focused ? (
              <Icon name="spotify" size={28} color="white" />
            ) : (
              <Icon name="spotify" size={25} color="#aaaaaa" />
            ),
        }}
      />
    </Tab.Navigator>
  );
};
const Stack = createStackNavigator();

const Navigation = () => {
  return (
    <NavigationContainer>
      <StatusBar style="light" backgroundColor="#020202" />
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          cardStyle: { backgroundColor: "#121212" },
          headerStyle: { backgroundColor: "#020202" },
          headerTintColor: "#fff",
          headerShadowVisible: false,
          cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
        }}
      >
        <Stack.Screen
          name="Login"
          component={Login}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Main"
          component={Bottomtabs}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="PlaySong"
          component={PlaySong}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="SongHistory"
          component={SongHistory}
          options={{ headerShown: true }}
        />
        <Stack.Screen
          name="Notification"
          component={Notification}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="Settings"
          component={Settings}
          options={{ headerShown: false }}
        />
        <Stack.Screen
          name="PlayListInfo"
          component={PlayListInfo}
          options={{
            headerShown: false,
          }}
        />
         <Stack.Screen
          name="LikedSongs"
          component={LikedSongs}
          options={{
            headerShown: true,
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
