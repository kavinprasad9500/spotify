import { Image, StyleSheet, Text, View } from "react-native";
import React from "react";
import Touchable from "../Touchable";
import { useNavigation } from "@react-navigation/native";

const List = (props) => {
  const navigation = useNavigation();

  return (
    <Touchable
      onPress={async () => {
        if (props.artist) {
          navigation.navigate("PlayListInfo", {
            artist: props.playlistDetails,
          });
          // console.log(props.playlistDetails.id);
        } else if (props.playlist) {
          navigation.navigate("PlayListInfo", {
            playlist: props.playlistDetails,
          });
        }
      }}
    >
      <View className="w-full h-16 flex-row items-center m-1">
        <Image
          source={{ uri: props.ImgUrl }}
          style={[{ width: 60, height: 60 }, props.ImgStyle]}
        />
        <View>
          <Text
            className="ml-4 text-white"
            numberOfLines={2}
            ellipsizeMode="tail"
          >
            {props.title}
          </Text>
          <Text
            className="ml-4 text-zinc-400 text-xs"
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            {props.type}
            {props.desc ? " • " + props.desc : null}
          </Text>
        </View>
      </View>
    </Touchable>
  );
};

export default List;
