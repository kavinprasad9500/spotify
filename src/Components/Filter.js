import { Text, View } from "react-native";
import React from "react";
import Touchable from "./Touchable";
import { Badge } from "native-base";

const Filter = (props) => {
  return (
    <View className="flex-row items-center my-3">
      <Touchable onPress={props.Btn1Press}>
        <Badge className="bg-stone-800 rounded-xl mr-3">
          <Text className="text-white text-sm">{props.Filter1}</Text>
        </Badge>
      </Touchable>
      <Touchable onPress={props.Btn2Press}>
        <Badge className="bg-stone-800 rounded-xl">
          <Text className="text-white text-sm">{props.Filter2}</Text>
        </Badge>
      </Touchable>
    </View>
  );
};

export default Filter;
