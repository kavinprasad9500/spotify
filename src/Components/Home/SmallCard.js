import { Image, ScrollView, StyleSheet, Text, View } from "react-native";
import React from "react";
import Touchable from "../Touchable";
import { useNavigation } from "@react-navigation/native";

const SmallCard = (props) => {
  const navigation = useNavigation();
  return (
    <>
      <Text className="text-white text-2xl font-extrabold">{props.title}</Text>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        className="my-3"
      >
        {props?.data?.map((item, index) => (
          <Touchable
            kwy={item?.track?.id}
           onPress={()=>navigation.navigate('PlaySong', { songId: item?.track?.id })}>
            {/* {console.log(item.track.id)} */}
            <View style={{ width: 120, height: 160 }} className="mr-3">
              <Image
                source={{ uri: item?.track?.album?.images[0]?.url }}
                style={{ width: 120, height: 120 }}
              />
              <Text
                numberOfLines={2}
                ellipsizeMode="tail"
                className="text-white pt-2 font-black text-xs"
              >
                {item?.track?.name}
                {"\n"}
              </Text>
            </View>
          </Touchable>
        ))}
      </ScrollView>
    </>
  );
};

export default SmallCard;
