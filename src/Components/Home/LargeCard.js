import { Image, ScrollView, Text, View } from "react-native";
import React from "react";
import Touchable from "../Touchable";
import { useNavigation } from "@react-navigation/native";
import { getAlbum } from '../../../config/Auth';

const LargeCard = (props) => {

  const navigateToAlbumDetails = async (albumId) => {
    try {
      const albumInfo = await getAlbum(albumId);
      // console.log(albumInfo.tracks.items[0].id);
      navigation.navigate("PlaySong", { songId: albumInfo?.tracks?.items[0]?.id });
    } catch (error) {
      console.error("Error fetching album:", error);
    }
  };

  const navigation = useNavigation();
  return (
    <>
      <Text className="text-white text-2xl font-extrabold mt-5">
        {props.title}
      </Text>

      <ScrollView
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        className="my-3"
      >
        {props?.data?.map((item, index) => (
          <Touchable
            key={item.id}
             onPress={async () => {
              if (props.song) {
                navigation.navigate("PlaySong", { songId: item?.id });
              } else if (props.album) {
                await navigateToAlbumDetails(item.id);
              } else if(props.artist){
                navigation.navigate('PlayListInfo' ,{artist : item});
                // console.log(item.id);
              }
            }}
          >
            {/* {console.log(item?.id)} */}
            <View style={{ width: 180, height: 220 }} className="mr-4">
              <Image
                source={{
                  uri: item?.album?.images[0]?.url || item?.images[0]?.url,
                }}
                style={{ width: 180, height: 170 }}
              />
              <Text
                numberOfLines={1}
                ellipsizeMode="tail"
                className="text-white pt-2 font-black text-xs"
              >
                {item.name} {"\n"}
              </Text>
              {item?.album_type ? (
                <Text
                  numberOfLines={1}
                  ellipsizeMode="tail"
                  className="text-neutral-400 pt-2 text-xs font-bold"
                >
                  {item?.album_type}
                  {" • "}
                  {item?.artists[0]?.name}
                </Text>
              ) : null}
            </View>
          </Touchable>
        ))}
      </ScrollView>
    </>
  );
};

export default LargeCard;
