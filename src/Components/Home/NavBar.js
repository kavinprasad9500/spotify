import { Text, View, StyleSheet } from "react-native";
import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import Touchable from "../Touchable";

const HomeNavBar = (props) => {
  const greetingMessage = () => {
    const currentTime = new Date().getHours();
    if (currentTime < 12) return "Good Morning";
    else if (currentTime < 16) return "Good Afternoon";
    else return "Good Evening";
  };
  const message = greetingMessage();

  return (
    <View className="flex-row items-center justify-between pt-10">
      <Text style={styles.greetingText}>{message}</Text>
      <View className="flex-row items-center">
        <Touchable onPress={props.PressNotification}>
          <Icon name="bell-o" size={25} color="white" />
        </Touchable>
        <Touchable onPress={props.PressHistory}>
          <Icon name="clock-o" size={25} color="white"
            style={{ marginLeft: 25 }}
          />
        </Touchable>
        <Touchable onPress={props.PressSettings}>
          <Icon
            name="gear"
            size={25}
            color="white"
            style={{ marginLeft: 25 }}
          />
        </Touchable>
      </View>
    </View>
  );
};

export default HomeNavBar;

const styles = StyleSheet.create({
  greetingText: {
    color: "white",
    fontSize: 24,
    fontWeight: "bold",
  },
});
