import { Image, ScrollView, Text, View } from "react-native";
import React from "react";
import Touchable from "../Touchable";

const MediumCard = (props) => {
  return (
    <>
      <Text className="text-white text-2xl font-extrabold">{props.title}</Text>
      <ScrollView
        showsHorizontalScrollIndicator={false}
        horizontal={true}
        className="my-3 mb-10"
      >
        {props?.data?.map((item, index) => (
          <Touchable key={item.id}>
            <View
              style={{ width: 160, height: 240 }}
              className="mr-4 rounded-xl"
            >
              <Image
                source={{ uri: item?.show?.images[0]?.url }}
                style={{ width: 160, height: 180 }}
                className="rounded-xl"
              />
              <Text
                numberOfLines={2}
                ellipsizeMode="tail"
                className="text-white pt-2 font-black text-xs"
              >
                {item?.show.name}
                {"\n"}
              </Text>
              <Text
                numberOfLines={2}
                ellipsizeMode="tail"
                className="text-lime-500 pt-2 text-xs font-bold"
              >
                {item?.show?.type}
              </Text>
            </View>
          </Touchable>
        ))}
      </ScrollView>
    </>
  );
};

export default MediumCard;
