import { Image, Text, View } from "react-native";
import React from "react";
import Touchable from "../Touchable";

const RecentList = (props) => {
  return (
    <Touchable style={[{width: '50%'}, props.style]} onPress={props.onPress}>
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "space-between",
          marginHorizontal: 4,
          marginVertical: 4,
          backgroundColor: "#282828",
          borderRadius: 5,
          elevation: 3,
        }}
      >
        <Image
          style={{ height: 55, width: 55, borderTopLeftRadius: 5, borderBottomLeftRadius: 5 }}
          source={{ uri: props?.imageUrl }}
        />
        <View
          style={{ flex: 1, marginHorizontal: 8, justifyContent: "center" }}
        >
          <Text
            numberOfLines={2}
            style={{ fontSize: 13, fontWeight: "bold", color: "white" }}
          >
            {props?.title}
          </Text>
        </View>
      </View>
    </Touchable>
  );
};

export default RecentList;
