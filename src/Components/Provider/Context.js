import React, { createContext, useState } from "react";

const AppContext = createContext();

const ContextProvider = ({ children }) => {
  const [currentTrack , setCurrentTrack] = useState(null);
  const [isLoading , setIsLoading] = useState(false);
  const [isPlayBackGround, setIsPLayBackGround] = useState(false);

  return (
    <AppContext.Provider value={{currentTrack, setCurrentTrack, isLoading, setIsLoading, isPlayBackGround, setIsPLayBackGround}}>
      {children}
    </AppContext.Provider>
  );
};

export {ContextProvider, AppContext} ;
