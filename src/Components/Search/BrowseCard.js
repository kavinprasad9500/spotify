import { Image, StyleSheet, Text, View } from "react-native";
import React from "react";
import Touchable from "../Touchable";

const BrowseCard = (props) => {
  return (
    <Touchable
      style={{ width: "50%" }}
      onPress={() => console.log("Categories Pressed")}
    >
      <View
        style={{
          flex: 1,
          flexDirection: "row",
          justifyContent: "space-between",
          marginHorizontal: 5,
          marginVertical: 5,
          backgroundColor: "#282828",
          borderRadius: 5,
          elevation: 3,
          height: 120,
        }}
      >
        <Image
          className="absolute rounded-lg"
          source={{ uri: props.image }}
          style={{ height: "100%", width: "100%" }}
        />
        <Text className="text-white font-black text-center w-full" style={{top: 90}}>{props.name}</Text>
      </View>
    </Touchable>
  );
};

export default BrowseCard;

const styles = StyleSheet.create({});
