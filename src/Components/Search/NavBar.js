import { StyleSheet, Text, View } from "react-native";
import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";

const SearchNavBar = () => {
  return (
    <View className="flex-row items-center justify-between py-3 pt-8">
      <Text style={styles.greetingText}>Search</Text>
      <View className="flex-row items-center">
        <Icon
          name="camera"
          size={25}
          color="white"
          style={{ marginLeft: 25 }}
        />
      </View>
    </View>
  );
};

export default SearchNavBar;


const styles = StyleSheet.create({
    greetingText: {
      color: "white",
      fontSize: 24,
      fontWeight: "bold",
    },
  });
  