import React, { useState } from "react";
import { Button } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import { Animated } from "react-native";

const MyButton = (props) => {
  const [animatedScale] = useState(new Animated.Value(1));
  const [animatedOpacity] = useState(new Animated.Value(1));

  const transIn = () => {
    Animated.parallel([
      Animated.spring(animatedScale, {
        toValue: 0.95,
        friction: 5,
        useNativeDriver: true,
      }),
      Animated.timing(animatedOpacity, {
        toValue: 0.8, // Lower opacity on press in
        duration: 200, // Duration of fade effect
        useNativeDriver: true,
      }),
    ]).start();
  };

  const transOut = () => {
    Animated.parallel([
      Animated.spring(animatedScale, {
        toValue: 1,
        friction: 5,
        useNativeDriver: true,
      }),
      Animated.timing(animatedOpacity, {
        toValue: 1, // Restore opacity on press out
        duration: 200, // Duration of fade effect
        useNativeDriver: true,
      }),
    ]).start();
  };

  return (
    <Animated.View style={{ transform: [{ scale: animatedScale }], width: '100%', opacity: animatedOpacity }}>
      <Button
        size="lg"
        style={[
          props.style,
          {
            borderRadius: 25,
            backgroundColor: props.backgroundColor,
            borderColor: "grey",
          },
        ]}
        w={"100%"}
        variant={props.variant}
        isDisabled={props.isDisabled}
        _text={{
          fontSize: props.fontSize,
          color: props.textColor,
          fontWeight: 900,
        }}
        leftIcon={
          props.icon ? (
            <Icon
              name={props.icon}
              size={20}
              color="white"
              style={{ marginRight: 10 }}
            />
          ) : null
        }
        onPress={props.onPress}
        onPressIn={transIn} // Add onPressIn event handler
        onPressOut={transOut} // Add onPressOut event handler
      >
        {props.title}
      </Button>
    </Animated.View>
  );
};

export default MyButton;
