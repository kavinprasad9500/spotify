import React, { useState } from "react";
import { Animated, Pressable } from "react-native";

const Touchable = (props) => {
  const [animatedScale] = useState(new Animated.Value(1));
  const [animatedOpacity] = useState(new Animated.Value(1));

  const transIn = () => {
    Animated.parallel([
      Animated.spring(animatedScale, {
        toValue: 0.95,
        friction: 5,
        useNativeDriver: true,
      }),
      Animated.timing(animatedOpacity, {
        toValue: 0.7, // Lower opacity on press in
        duration: 200, // Duration of fade effect
        useNativeDriver: true,
      }),
    ]).start();
  };

  const transOut = () => {
    Animated.parallel([
      Animated.spring(animatedScale, {
        toValue: 1,
        friction: 5,
        useNativeDriver: true,
      }),
      Animated.timing(animatedOpacity, {
        toValue: 1, // Restore opacity on press out
        duration: 200, // Duration of fade effect
        useNativeDriver: true,
      }),
    ]).start();
  };

  return (
    <Pressable style={props.style} onPressIn={transIn} onPressOut={transOut} onPress={props.onPress}>
      <Animated.View
        style={{
          transform: [{ scale: animatedScale }],
          opacity: animatedOpacity,
        }}
      >
        {props.children}
      </Animated.View>
    </Pressable>
  );
};

export default Touchable;
