import { View, Text } from "react-native";
import React, { useEffect } from "react";
import * as AppAuth from "expo-app-auth";
import * as AuthSession from "expo-auth-session";
import AsyncStorage from "@react-native-async-storage/async-storage";
import * as Google from "expo-auth-session/providers/google";

import { useAuthRequest, makeRedirectUri } from "expo-auth-session";
import * as WebBrowser from "expo-web-browser";
import axios from "axios";
import base64 from "base-64";
const Authentication = () => {
  WebBrowser.maybeCompleteAuthSession();
  const discovery = {
    authorizationEndpoint: "https://accounts.spotify.com/authorize",
    tokenEndpoint: "https://accounts.spotify.com/api/token",
  };
  // const redirectUri = makeRedirectUri({
  //   scheme: 'https',
  //   host: 'spotify.kavinprasad.me',
  //   path: '/auth', // Replace with the path you want to use
  // });

  const [request, response, promptAsync] = useAuthRequest(
    {
      clientId: "a1583ea413ac4defbc120a8a5a86bca4",
      scopes: [
        "user-read-email",
        "user-library-read",
        "user-read-recently-played",
        "user-top-read",
        "playlist-read-private",
        "playlist-read-collaborative",
        "playlist-modify-public", // or "playlist-modify-private"
      ],
      // In order to follow the "Authorization Code Flow" to fetch token after authorizationEndpoint
      // this must be set to false
      usePKCE: false,
      redirectUri: makeRedirectUri({
        scheme: "exp://192.168.245.117:8081",
      }),
    },
    discovery
  );

  (async function setresponse() {
    if (response) {
      if (response.error) {
        Alert.alert(
          "Authentication error",
          response.params.error_description || "something went wrong"
        );
        return;
      }
      if (response.type === "success") {
        // getAccessToken(response.params.code, response.params.state);
        // console.log("Code : "+response.params.code);
        // console.log("State : "+response.params.state);
        // set Code and State
      }
    }
  })();
};

const getAccessToken = async (code, state) => {
  const client_id = "a1583ea413ac4defbc120a8a5a86bca4";
  const redirect_uri = "exp://192.168.150.117:8081";
  const client_secret = "68a0a96e433343b7ae20c43fa8c2aeec";

  if (state === null) {
    // Handle state mismatch error
    return { error: "State mismatch" };
  } else {
    const authOptions = {
      url: "https://accounts.spotify.com/api/token",
      method: "post",
      data: `code=${code}&redirect_uri=${encodeURIComponent(
        redirect_uri
      )}&grant_type=authorization_code`,
      headers: {
        Authorization: `Basic ${base64.encode(
          `${client_id}:${client_secret}`
        )}`,
      },
    };

    try {
      // Make the token exchange request and return the response
      const response = await axios(authOptions);
      // console.log(response.data);
      // Handle the response here (e.g., store tokens, navigate to next screen)
      return response.data;
    } catch (error) {
      console.log(error);
      // Handle error
      return { error: "Token exchange error" };
    }
  }
};

const getRecentlyPlayedSongs = async (limit) => {
  try {
    let limits = limit;
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/me/player/recently-played?limit=${limit}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    const tracks = response.data.items;
    // const uniqueTracks = [];
    // const trackIds = new Set();
    // for (const track of tracks) {
    //   if (!trackIds.has(track.track.id)) {
    //     uniqueTracks.push(track);
    //     trackIds.add(track.track.id);
    //   }
    // }

    // return uniqueTracks;
    // // console.log(tracks);
    return tracks;
    // console.log(tracks);
  } catch (err) {
    console.log(err.message);
  }
};

const getRecommended = async () => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const url =
      "https://api.spotify.com/v1/recommendations?limit=1&seed_artists=4NHQUGzhtTLFvgF5SZesLK&seed_genres=classical%2Ccountry&seed_tracks=0c6xIDDpzE81m2q797ordA";

    const response = await axios({
      method: "GET",
      url: url,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    // const recommended = response; // Use 'tracks' property for recommended songs
    // console.log(response);
    // return recommended;
  } catch (err) {
    console.log(err.message);
    // return []  ; // Return an empty array or handle the error case
  }
};

const browseCategories = async () => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: "https://api.spotify.com/v1/browse/categories?country=IN",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    const categories = response.data.categories.items;
    // categories.forEach(category => {
    //   console.log(category.name);
    //   console.log(category.icons[0].url);
    // });
    return categories;
    // console.log(tracks);
  } catch (err) {
    console.log(err.message);
  }
};

const getUserPlayList = async () => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: "https://api.spotify.com/v1/me/playlists",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    const playlist = response.data.items;
    // console.log(playlist);
    return playlist;
  } catch (err) {
    console.log(err.message);
  }
};

const getUser = async () => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: "https://api.spotify.com/v1/me",
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    const user = response.data;
    // console.log(user);
    return user;
    // console.log(tracks);
  } catch (err) {
    console.log(err.message);
  }
};

const getFollow = async (type) => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/me/following?type=${type}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    const data = response.data;
    // console.log(getfollow.artists.items[0].name);
    return data;
    // console.log(tracks);
  } catch (err) {
    console.log(err.message);
  }
};

const getTop = async (type) => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/me/top/${type}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    const data = response.data.items;
    // console.log(data);
    return data;
    // console.log(tracks);
  } catch (err) {
    console.log(err.message);
  }
};

const newReleaseList = async () => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/browse/new-releases?country=IN&limit=15`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    const data = response.data.albums.items;
    // console.log(data);
    return data;
    // console.log(tracks);
  } catch (err) {
    console.log(err.message);
  }
};

const getUserPodcast = async () => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/me/shows`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    const data = response.data;
    // console.log(data.items[0].show.id);
    // console.log(data.items[1].show.id);
    // console.log(data.items[2].show.id);
    return data.items;
    // console.log(tracks);
  } catch (err) {
    console.log(err.message);
  }
};

const fetchSong = async (id) => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/tracks/${id}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    // console.log("Called");
    const data = response.data;
    // console.log(data);
    return data;
  } catch (err) {
    console.log(err);
  }
};

const userProfile = async () => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/me`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    // console.log("Called");
    const data = response.data;
    // console.log(data);
    return data;
  } catch (err) {
    console.log(err);
  }
};


const getFollowArtist = async () => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/me/following?type=artist`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    // console.log("Called");
    const data = response.data;
    // console.log(data);
    return data;
  } catch (err) {
    console.log(err);
  }
};

const getPlayListDetails = async (playlistId) => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/playlists/${playlistId}/tracks`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    // console.log("Called");
    const data = response.data;
    // console.log(data);
    return data;
  } catch (err) {
    console.log(err);
  }
};


const getAlbum = async (albumId) => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/albums/${albumId}`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    // console.log("Called");
    const data = response.data;
    return data;
  } catch (err) {
    console.log(err);
  }
};


const searchTrack = async (searchText) => {
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/search?q=${searchText}&type=track&limit=15`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    // console.log("Called");
    const data = response.data;
    return data;
  } catch (err) {
    console.log(err);
  }
};

const getArtistTopTracks = async(artistId) =>{
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/artists/${artistId}/top-tracks?market=IN`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    // console.log("Called");
    const data = response.data;
    // console.log(data);
    return data;
  } catch (err) {
    console.log(err);
  }
}

const userLikedSongs = async () =>{
  try {
    const accessToken = await AsyncStorage.getItem("token");
    const response = await axios({
      method: "GET",
      url: `https://api.spotify.com/v1/me/tracks`,
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
    // console.log("Called");
    const data = response.data;
    // console.log(data);
    return data;
  } catch (err) {
    console.log(err);
  }
}

export {
  Authentication,
  getAccessToken,
  getRecentlyPlayedSongs,
  getRecommended,
  browseCategories,
  getUser,
  getUserPlayList,
  getFollow,
  getTop,
  newReleaseList,
  getUserPodcast,
  userProfile,
  getFollowArtist,
  getPlayListDetails,
  fetchSong,
  getAlbum,
  searchTrack,
  userLikedSongs,
  getArtistTopTracks
};



// import axios from "axios";
// import AsyncStorage from "@react-native-async-storage/async-storage";
// import base64 from "base-64";

// class SpotifyAPI {
//   constructor() {
//     this.accessToken = null;
//   }

//   async setAccessToken() {
//     this.accessToken = await AsyncStorage.getItem("token");
//   }

//   async makeAuthorizedRequest(url, method = "GET") {
//     try {
//       const response = await axios({
//         method,
//         url,
//         headers: {
//           Authorization: `Bearer ${this.accessToken}`,
//         },
//       });
//       return response.data;
//     } catch (error) {
//       console.log(error.message);
//       throw error;
//     }
//   }

//   async getRecentlyPlayedSongs(limit) {
//     await this.setAccessToken();
//     const url = `https://api.spotify.com/v1/me/player/recently-played?limit=${limit}`;
//     return this.makeAuthorizedRequest(url);
//   }

//   async getRecommended() {
//     await this.setAccessToken();
//     const url = "https://api.spotify.com/v1/recommendations?limit=1&seed_artists=4NHQUGzhtTLFvgF5SZesLK&seed_genres=classical%2Ccountry&seed_tracks=0c6xIDDpzE81m2q797ordA";
//     return this.makeAuthorizedRequest(url);
//   }

//   // Define other methods similarly
// }

// export default SpotifyAPI;

// import SpotifyAPI from "./SpotifyAPI"; // Update the path accordingly

// const spotifyAPI = new SpotifyAPI();


// Get recently played songs
// const recentlyPlayed = await spotifyAPI.getRecentlyPlayedSongs(10);
// console.log("Recently Played Songs:", recentlyPlayed);

// // Get recommended tracks
// const recommended = await spotifyAPI.getRecommended();
// console.log("Recommended Tracks:", recommended);

// // You can call other methods in a similar way
