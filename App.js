import React from "react";
import {ContextProvider} from "./src/Components/Provider/Context";
import Navigation from "./src/Components/Navigation/Navigation";
import { NativeBaseProvider } from "native-base";

export default function App() {
  return (
    <NativeBaseProvider>
      <ContextProvider>
        <Navigation />
      </ContextProvider>
    </NativeBaseProvider>
  );
}
